const path = require("path")
const EsmWebpackPlugin = require("@purtuga/esm-webpack-plugin")
const CopyWebpackPlugin = require('copy-webpack-plugin')

const clientFiles = [
  'common-client-plugin.js'
]

const config = clientFiles.map(f => ({
  entry: "./client/" + f,
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "./" + f,
    library: "script",
    libraryTarget: "var"
  },
  plugins: [
    new EsmWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: './client/one-signal',
          to: './',
        },
      ],
    }),
  ]
}))

module.exports = config
