const path = require('path');
const packageJson = require('./package.json');
const pluginName = packageJson.name.replace('peertube-plugin-', '');

async function register ({
  getRouter,
  registerHook,
  registerSetting,
  settingsManager,
  storageManager,
  videoCategoryManager,
  videoLicenceManager,
  videoLanguageManager
}) {
  registerSetting({
    name: 'appId',
    label: 'OneSignal app id',
    type: 'input',
    descriptionHTML: '<a href="https://documentation.onesignal.com/docs/accounts-and-keys" target="_blank">OneSignal app id</a>',
    private: false,
  });

  registerSetting({
    name: 'subdomainName',
    label: 'Subdomain name',
    type: 'input',
    descriptionHTML: '<a href="https://documentation.onesignal.com/docs/web-push-sdk#init" target="_blank">OneSignal subdomain name</a>',
    private: false,
  });

  registerSetting({
    name: 'actionMessage',
    label: 'OneSignal action message',
    type: 'input',
    descriptionHTML: '<a href="https://documentation.onesignal.com/docs/web-push-sdk#init-promptoptions-parameters" target="_blank">Action message to show in prompt dialog.</a>',
    private: false,
  });

  registerSetting({
    name: 'acceptButton',
    label: 'OneSignal accept button',
    type: 'input',
    descriptionHTML: '<a href="https://documentation.onesignal.com/docs/web-push-sdk#init-promptoptions-parameters" target="_blank">Accept button to show in prompt dialog.</a>',
    private: false,
  });

  registerSetting({
    name: 'cancelButton',
    label: 'OneSignal cancel button',
    type: 'input',
    descriptionHTML: '<a href="https://documentation.onesignal.com/docs/web-push-sdk#init-promptoptions-parameters" target="_blank">Cancel button to show in prompt dialog.</a>',
    private: false,
  });

  registerSetting({
    name: 'welcomeNotificationTitle',
    label: 'Welcome notification title',
    type: 'input',
    descriptionHTML: '<a href="https://documentation.onesignal.com/docs/accounts-and-keys" target="_blank">Title in welcome notification shown after subscription.</a>',
    private: false,
  });

  registerSetting({
    name: 'welcomeNotificationMessage',
    label: 'Welcome notification message',
    type: 'input',
    descriptionHTML: '<a href="https://documentation.onesignal.com/docs/accounts-and-keys" target="_blank">Title in welcome notification shown after subscription.</a>',
    private: false,
  });

  const router = getRouter();

  [
    'OneSignalSDKUpdaterWorker.js',
    'OneSignalSDKWorker.js',
  ].forEach(filename =>
    router.get(`/${filename}`, (req, res) =>
      res
        .set({
          'Service-Worker-Allowed': `/plugins/${pluginName}`,
        })
        .sendFile(path.resolve(__dirname, 'dist', filename))
      )
  );
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
