# OneSignal integration to PeerTube

Basic functionality to integrate OneSignal with PeerTube. Based on https://documentation.onesignal.com/docs/onesignal-service-worker-faq

## Getting started

1) Ensure that your OneSong app _Web configuration_ is set to _Custom Code_.
1) Install the plugin.
1) Enter your configuration in the plugin settings.
1) You're ready to go.

## Contribute
PR's are welcome.
