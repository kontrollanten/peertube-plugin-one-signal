const packageJson = require('../package.json');

async function register({ peertubeHelpers }) {
  const settings = await peertubeHelpers.getSettings();

  if (!settings['appId']) {
    console.warn(`${packageJson.name}: No app id found.`);
    return;
  }

  const script = document.createElement('script');
  script.src = 'https://cdn.onesignal.com/sdks/OneSignalSDK.js';
  script.async = 'true';
  document.body.appendChild(script);

  window.OneSignal = window.OneSignal || [];
  window.OneSignal.push(async () => {
    const shortName = packageJson.name.replace('peertube-plugin-', '');
    window.OneSignal.SERVICE_WORKER_PARAM = {
      scope: `/plugins/${shortName}/`,
    };
    const swPath = `plugins/${shortName}/${packageJson.version}/router`;
    window.OneSignal.SERVICE_WORKER_PATH = `${swPath}/OneSignalSDKWorker.js`;
    window.OneSignal.SERVICE_WORKER_UPDATER_PATH = `${swPath}/OneSignalSDKUpdaterWorker.js`;

    window.OneSignal.init({
      appId: settings['appId'],
      allowLocalhostAsSecureOrigin: true,
      notifyButton: {
        enable: true
      },
      subdomainName: settings.subdomainName,
      promptOptions: {
        slidedown: {
          prompts: [
            {
              type: 'push',
              autoPrompt: true,
              text: {
                actionMessage: settings.actionMessage,
                acceptButton: settings.acceptButton,
                cancelButton: settings.cancelButton,
              },
              delay: {
                pageViews: 1,
                timeDelay: 2,
              }
            }
          ]
        }
      },
      welcomeNotification: {
        title: settings.welcomeNotificationTitle,
        message: settings.welcomeNotificationMessage,
      },
    });
  });

  if ('gtag' in window) {
    window.OneSignal.push(() => {
      [
        'popoverShown',
        'popoverAllowClick',
        'popoverCancelClick',
      ].forEach(event =>
        window.OneSignal.on(event, () => gtag('event', event))
      )
    });
  }
}

export {
  register
}
